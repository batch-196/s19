let username, password, role;
function checkForm(username, password, role) {

    username = prompt("Please enter your Username", "Username");
    if (username === "" || username === null) {
        alert("The input should not be empty");
    } else {
        password = prompt("Please enter your Password", "Password");
        if (password === "" || password === null) {
            alert("The input should not be empty");
        } else {
            role = prompt("Please enter your Role", "Role").toLowerCase();
            if (role === "" || role === null) {
                alert("The input should not be empty");
            } else {
                switch (role) {
                    case ("admin"):
                        alert("Welcome back to the class portal, admin!");
                        return username + password + role;

                    case ("teacher"):
                        alert("Thank you for logging in, teacher!");
                        return username + password + role;

                    case ("student"):
                        alert("Welcome to the class portal, student!");
                        return username + password + role;

                    default:
                        alert("Role out of range.");

                }
            }
        }
    }
}
checkForm(username, password, role);
//=======================
// Average
//=======================
let num1, num2, num3, num4, average;
num1 = prompt("Please enter the First Number");
num2 = prompt("Please enter the Second Number");
num3 = prompt("Please enter the Third Number");
num4 = prompt("Please enter the Fourth Number");

function getAverage(num1, num2, num3, num4) {
    average = (num1 + num2 + num3 + num4) / 4;
    Math.round(average);
    return average;
}

getAverage(num1, num2, num3, num4)

if (average <= 74) {
    console.log("Hello Student, your Average is " + average + ". The letter equivalent is F");
} else if (average >= 75 && average <= 79) {
    console.log("Hello Student, your Average is " + average + ". The letter equivalent is D");
} else if (average >= 80 && average <= 84) {
    console.log("Hello Student, your Average is " + average + ". The letter equivalent is C");
} else if (average >= 85 && average <= 89) {
    console.log("Hello Student, your Average is " + average + ". The letter equivalent is B");
} else if (average >= 90 && average <= 95) {
    console.log("Hello Student, your Average is " + average + ". The letter equivalent is A");
} else if (average >= 96) {
    console.log("Hello Student, your Average is " + average + ". The letter equivalent is A+");
}
